const express = require('express');
const crud = require('./controller/crud.js');
const User = require('./controller/user.js');
const user = new User();
const router = express.Router();
const path = require('path');
const notifier = require('node-notifier');
const fs = require('fs');
const json_books = fs.readFileSync('datos.json', 'utf-8');
let books = JSON.parse(json_books);


const conexion = require('./databases/db');

router.get('/', (req, res)=>{     
    
    res.sendFile(path.join(__dirname, '/index.html'));
})
/**
 * Ruta de html
 *  */
router.get('/registro.html', (req,res)=>{
    res.sendFile(path.join(__dirname, './registro.html'));
    
})
router.get('https://kit.fontawesome.com/41bcea2ae3.js', (req,res)=>{
    res.sendFile(path.join(__dirname, '.https://kit.fontawesome.com/41bcea2ae3.js'));
    console.log("registro");
})


/**
 * Ruta de css
 *  */
router.get('/css/principal.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/principal.css'));
   
})
router.get('/css/principal2.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/principal2.css'));
   
})
router.get('/css/articulos.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/articulos.css'));
 
})


router.get('/css/ficha.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/ficha.css'));
 
})
router.get('/css/icons.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/icons.css'));
 
})
router.get('/css/book-container.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/book-container.css'));
 
})
router.get('/css/navbar.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/navbar.css'));
 
})



router.get('/css/styles_ficha.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/styles_ficha.css'));
 
})
router.get('/css/style.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/style.css'));
 
})
router.get('/css/styles_ini.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/styles_ini.css'));
 
})
router.get('/css/cookie.css', (req,res)=>{
    res.sendFile(path.join(__dirname, './css/cookie.css'));
    
    
    router.get('/css/Inicio.css', (req,res)=>{
        res.sendFile(path.join(__dirname, '/css/Inicio.css'));
       
    })
    
    router.get('/css/diseño.css', (req, res) => {
        res.sendFile(path.join(__dirname, '/css/diseño.css'));
      });
      router.get('/css/grupo.css', (req, res) => {
        res.sendFile(path.join(__dirname, '/css/grupo.css'));
      });
    
    
})



/**
 * Ruta de javascript
 *  */



router.get('/js/form.js', (req,res)=>{
    res.sendFile(path.join(__dirname, '/js/form.js'));
    
   
})




/**
 * Ruta de imagenes
 *  */
 router.get('/img/1.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/1.jpg'));
   
})
router.get('/img/2.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/2.jpg'));
   
})
router.get('/img/3.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/2.jpg'));
   
})
router.get('/img/4.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/4.jpg'));
   
})
router.get('/img/5.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/5.jpg'));
   
})
router.get('/img/6.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/6.jpg'));
   
})
router.get('/img/7.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/7.jpg'));
   
})

router.get('/img/img6.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/img6.jpg'));
   
})
router.get('/img/img5.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/img5.jpg'));
   
})
router.get('/img/img4.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/img4.jpg'));
   
})
router.get('/img/img3.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/img3.jpg'));
   
})
router.get('/img/img2.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/img2.jpg'));
   
})
router.get('/img/img1.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/img1.jpg'));
   
})
router.get('/img/img1.jpg', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/img1.jpg'));
   
})
router.get('/img/avatar.png', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/avatar.png'));
   
})

router.get('/img/a1.png', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/a1.png'));
   
})
router.get('/img/a2.png', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/a2.png'));
   
})
router.get('/img/StudetMaleAvatar.png', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/StudetMaleAvatar.png'));
   
})
router.get('/img/a3.png', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/a3.png'));
   
})

router.get('/img/close.png', (req,res)=>{
    res.sendFile(path.join(__dirname, '/img/close.png'));
   
})








  









router.post('/save', crud.save);



module.exports = router;